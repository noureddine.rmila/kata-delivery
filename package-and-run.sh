#!/bin/bash

# Run Maven package command
./mvnw clean package

# Run Docker compose
docker-compose up
