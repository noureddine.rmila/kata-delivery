# Carrefour Java kata -- Delivery

# Spring Boot 3 Project with Reactive WebFlux and Hexagonal Architecture

## Overview
This project is a Java 21, Spring Boot 3 application built with Reactive WebFlux and follows a hexagonal architecture pattern. It provides services through a RESTful API.

## Project Structure
The project follows a hexagonal architecture, separating business logic from delivery mechanisms. It consists of the following components:
- **Application Layer**: Contains input and output ports for delivery and time slot services.
- **Domain Layer**: Defines the domain entities such as Delivery and TimeSlot.
- **Infrastructure Layer**: Implements the repository interfaces and database configurations.
- **Presentation Layer**: Exposes REST endpoints for delivery and time slot operations.

## Database Versioning
[Liquibase](https://www.liquibase.org/) is used for database versioning. It helps manage and apply database schema changes as code.

## Docker Compose
A Docker Compose file is provided to facilitate quick setup and development. It includes configurations for the database and other dependencies.

## Native Image Build
The project includes a plugin for building a native image using GraalVM. This enables faster startup times and reduced resource usage.

## Docker Image
A Dockerfile is provided to build a Docker image for the application. It includes configurations for building the application's native image and setting up the runtime environment.


## Docker Compose File
A Docker Compose file (`docker-compose.yml`) is included in the project to help set up the development environment quickly. It defines services for the database and any other dependencies required by the application. Also kick start for local dev ( Redis, Kafka, PostgreSQL)

```yaml
com/carrefour/deliveryservice/container/dev/kafka.yml
com/carrefour/deliveryservice/container/dev/postgresql.yml
com/carrefour/deliveryservice/container/dev/redis.yml
```
------


> Vous trouverez ici un exercice assez libre d'implémentation à réaliser dans un temps restreint.\
> Il a pour objectif d'ouvrir la discussion lors d'un futur entretien.\
> Nous vous conseillons de ne pas y passer plus de 2h même si vous n'avez pas eu le temps de finir les User Stories obligatoires.

## Consignes
L'exercice est composé d'une partie obligatoire, le [Minimum Valuable Product](#mvp).\
Il y a également des [fonctionnalités facultatives](#features-bonus) afin d'utiliser le temps restant pour vous démarquer.\
Les stories ne possèdent pas de critères d'acceptance, c'est à vous de les définir après votre analyse fonctionnelle de la story.

**S'il vous manque une information, faites un choix et restez cohérent avec celui-ci.**

### Contraintes
- Spring-boot 3.2.x
- Java 21
- Git
- Fichier `README.md` -- _Explique les potentielles subtilités de votre implémentation et comment lancer votre projet_.

### Livraison
Le code devrait être disponible sur un repository [GitLab](https://gitlab.com).

### Evaluation
**Il n'y a pas de "bonne" façon de réaliser cet exercice.**\
Nous sommes intéressés par vos choix d'implémentations, votre technique, l'architecture du code et le respect des contraintes.\
_Faites également attention à la taille de vos commits et leurs messages._

### Tips
Pour créer rapidement votre base de projet, utilisez [spring initializr](https://start.spring.io/)

## Exercice
### MVP
#### User Story
> En tant que client, je peux choisir mon mode de livraison.\
> Les modes de livraison disponibles sont : `DRIVE`, `DELIVERY`, `DELIVERY_TODAY`, `DELIVERY_ASAP`.

#### User Story
> En tant que client, je peux choisir mon jour et mon créneau horaire.\
> Les créneaux sont spécifiques au mode de livraison et réservable par d'autres clients.

### Features Bonus
Les fonctionnalités suivantes sont optionnelles et non exhaustives.\
Elles n'ont pas de priorité entre elles, vous pouvez implémenter celles qui vous intéressent ou en proposer d'autres.

#### API REST
- Proposer une API REST consommable via http pour interagir avec les services réalisés dans le MVP:heavy_check_mark:
- Implémenter les principes HATEOAS dans votre API REST:x:
- Sécuriser l'API:x:
- Utiliser une solution non-bloquante:heavy_check_mark:
- Documenter l'API REST:heavy_check_mark:

#### Persistence
- Proposer une solution de persistence des données:heavy_check_mark:
- Proposer une solution de cache:x:

#### Stream# Carrefour Java kata -- Delivery

# Spring Boot 3 Project with Reactive WebFlux and Hexagonal Architecture

## Overview
This project is a Spring Boot 3 application built with Reactive WebFlux and follows a hexagonal architecture pattern. It provides delivery-related services through a RESTful API.

## Project Structure
The project follows a hexagonal architecture, separating business logic from delivery mechanisms. It consists of the following components:
- **Application Layer**: Contains input and output ports for delivery and time slot services.
- **Domain Layer**: Defines the domain entities such as Delivery and TimeSlot.
- **Infrastructure Layer**: Implements the repository interfaces and database configurations.
- **Presentation Layer**: Exposes REST endpoints for delivery and time slot operations.

## Database Versioning
[Liquibase](https://www.liquibase.org/) is used for database versioning. It helps manage and apply database schema changes as code.

## Docker Compose
A Docker Compose file is provided to facilitate quick setup and development. It includes configurations for the database and other dependencies.

## Native Image Build
The project includes a plugin for building a native image using GraalVM. This enables faster startup times and reduced resource usage.

## Docker Image
A Dockerfile is provided to build a Docker image for the application. It includes configurations for building the application's native image and setting up the runtime environment.


## Docker Compose File
A Docker Compose file (`docker-compose.yml`) is included in the project to help set up the development environment quickly. It defines services for the database and any other dependencies required by the application.

```yaml
com/carrefour/deliveryservice/container/dev/kafka.yml
com/carrefour/deliveryservice/container/dev/postgresql.yml
com/carrefour/deliveryservice/container/dev/redis.yml
```

### How to run :
Start the databse:
```
docker-compose -f postgresql.yml up -d 
```
Run the project: 
```
# Run Maven package command
./mvnw clean package

# Run Docker compose
docker-compose up

or 

./package-and-run.sh
```
------


> Vous trouverez ici un exercice assez libre d'implémentation à réaliser dans un temps restreint.\
> Il a pour objectif d'ouvrir la discussion lors d'un futur entretien.\
> Nous vous conseillons de ne pas y passer plus de 2h même si vous n'avez pas eu le temps de finir les User Stories obligatoires.

## Consignes
L'exercice est composé d'une partie obligatoire, le [Minimum Valuable Product](#mvp).\
Il y a également des [fonctionnalités facultatives](#features-bonus) afin d'utiliser le temps restant pour vous démarquer.\
Les stories ne possèdent pas de critères d'acceptance, c'est à vous de les définir après votre analyse fonctionnelle de la story.

**S'il vous manque une information, faites un choix et restez cohérent avec celui-ci.**

### Contraintes
- Spring-boot 3.2.x
- Java 21
- Git
- Fichier `README.md` -- _Explique les potentielles subtilités de votre implémentation et comment lancer votre projet_.

### Livraison
Le code devrait être disponible sur un repository [GitLab](https://gitlab.com).

### Evaluation
**Il n'y a pas de "bonne" façon de réaliser cet exercice.**\
Nous sommes intéressés par vos choix d'implémentations, votre technique, l'architecture du code et le respect des contraintes.\
_Faites également attention à la taille de vos commits et leurs messages._

### Tips
Pour créer rapidement votre base de projet, utilisez [spring initializr](https://start.spring.io/)

## Exercice
### MVP
#### User Story
> En tant que client, je peux choisir mon mode de livraison.\
> Les modes de livraison disponibles sont : `DRIVE`, `DELIVERY`, `DELIVERY_TODAY`, `DELIVERY_ASAP`.

#### User Story
> En tant que client, je peux choisir mon jour et mon créneau horaire.\
> Les créneaux sont spécifiques au mode de livraison et réservable par d'autres clients.

### Features Bonus
Les fonctionnalités suivantes sont optionnelles et non exhaustives.\
Elles n'ont pas de priorité entre elles, vous pouvez implémenter celles qui vous intéressent ou en proposer d'autres.

#### API REST
- Proposer une API REST consommable via http pour interagir avec les services réalisés dans le MVP :heavy_check_mark:
- Implémenter les principes HATEOAS dans votre API REST :x:
- Sécuriser l'API :x:
- Utiliser une solution non-bloquante :heavy_check_mark:
- Documenter l'API REST :heavy_check_mark:

#### Persistence
- Proposer une solution de persistence des données :heavy_check_mark:
- Proposer une solution de cache :x:

#### Stream
- Proposer une solution de streaming de données :x:
- Proposer une solution de consommation et/ou production d'évènements :x:

### CI/CD
- Proposer un system de CI/CD pour le projet :x:
- Proposer des tests End to End à destination de votre application :x:

### Packaging
- Créer un container de votre application :heavy_check_mark:
- Déployer votre application dans un pod :x:
- Créer une image native de votre application   :heavy_check_mark:

- Proposer une solution de streaming de données :x:
- Proposer une solution de consommation et/ou production d'évènements :x:

### CI/CD
- Proposer un system de CI/CD pour le projet:x:
- Proposer des tests End to End à destination de votre application:x:

### Packaging
- Créer un container de votre application:heavy_check_mark:
- Déployer votre application dans un pod:x:
- Créer une image native de votre application :heavy_check_mark:
