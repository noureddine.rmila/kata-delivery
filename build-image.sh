#!/bin/bash

# Run Maven package command
./mvnw clean package

# Build Docker image
docker build --build-arg JAR_FILE=target/*.jar -t rmila/delivery-kata .
