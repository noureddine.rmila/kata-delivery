package com.rmila.deliverykata.presentation.exception;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import static org.springframework.http.ResponseEntity.notFound;

@ControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

    @ExceptionHandler(DeliveryModeNotFoundException.class)
    ResponseEntity postNotFound(DeliveryModeNotFoundException ex) {
        log.debug("handling exception::" + ex);
        return notFound().build();
    }
}
