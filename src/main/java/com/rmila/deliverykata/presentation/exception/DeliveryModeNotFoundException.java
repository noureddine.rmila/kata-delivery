package com.rmila.deliverykata.presentation.exception;

public class DeliveryModeNotFoundException extends RuntimeException {
    public DeliveryModeNotFoundException(Long id) {
        super("Delivery modes:" + id +" is not found.");
    }
}