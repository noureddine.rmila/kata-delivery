package com.rmila.deliverykata.presentation.delivery.dto;

import com.rmila.deliverykata.domain.delivery.DeliveryMode;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DeliveryDTO {
    private Long id;
    private DeliveryMode name;
    private String description;
}
