package com.rmila.deliverykata.presentation.delivery.controller;

import com.rmila.deliverykata.application.port.timeSlot.TimeSlotServiceInputPort;
import com.rmila.deliverykata.presentation.delivery.dto.TimeSlotDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Controller for managing time slot-related operations.
 * Exposes REST endpoints for reserving time slots and retrieving available time slots by mode.
 * @author Noureddine Rmila
 */
@RestController
@RequestMapping("/time-slots")
public class TimeSlotController {

    private final TimeSlotServiceInputPort timeSlotService;

    public TimeSlotController(TimeSlotServiceInputPort timeSlotService) {
        this.timeSlotService = timeSlotService;
    }

    /**
     * Reserves a time slot for a user.
     * @param timeSlotId the ID of the time slot to reserve.
     * @param userId the ID of the user reserving the time slot.
     * @return a {@link reactor.core.publisher.Mono} emitting a {@link org.springframework.http.ResponseEntity} containing a success message if the reservation is successful,
     *         or a bad request message if the reservation fails.
     */
    @PostMapping("/reserve")
    public Mono<ResponseEntity<String>> reserveTimeSlot(@RequestParam Long timeSlotId, @RequestParam Long userId) {
        return timeSlotService.reserveTimeSlot(timeSlotId, userId)
                .flatMap(result -> {
                    if (result.getT1()) {
                        return Mono.just(ResponseEntity.ok(result.getT2()));
                    } else {
                        return Mono.just(ResponseEntity.badRequest().body(result.getT2()));
                    }
                })
                .switchIfEmpty(Mono.just(ResponseEntity.notFound().build()));
    }

    /**
     * Retrieves available time slots for a given delivery mode.
     * @param id the ID of the delivery mode.
     * @return a {@link reactor.core.publisher.Flux} emitting TimeSlotDTOs.
     */
    @GetMapping("/{id}")
    public Flux<TimeSlotDTO> getTimeSlotsByMode(@PathVariable Long id) {
        return timeSlotService.getAvailableTimeSlotsByMode(id);
    }
}
