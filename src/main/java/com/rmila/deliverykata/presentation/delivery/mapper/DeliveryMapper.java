package com.rmila.deliverykata.presentation.delivery.mapper;

import com.rmila.deliverykata.domain.delivery.Delivery;
import com.rmila.deliverykata.presentation.delivery.dto.DeliveryDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface DeliveryMapper {
    DeliveryDTO deliveryToDTO(Delivery delivery);
    Delivery deliveryDTOTodelivery(DeliveryDTO deliveryDTO);
}
