package com.rmila.deliverykata.presentation.delivery.controller;

import com.rmila.deliverykata.application.port.delivery.DeliveryServiceInputPort;
import com.rmila.deliverykata.presentation.delivery.dto.DeliveryDTO;
import com.rmila.deliverykata.presentation.exception.DeliveryModeNotFoundException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Controller for managing delivery-related operations.
 * Exposes REST endpoints for retrieving delivery modes and individual deliveries.
 * @author Noureddine Rmila
 */
@RestController
@RequestMapping("/delivery")
public class DeliveryController {

    private final DeliveryServiceInputPort deliveryService;

    public DeliveryController(DeliveryServiceInputPort deliveryService) {
        this.deliveryService = deliveryService;
    }

    /**
     * Retrieves all delivery modes.
     * @return a {@link reactor.core.publisher.Flux} emitting DeliveryDTOs.
     */
    @GetMapping("/delivery-modes")
    public Flux<DeliveryDTO> getDeliveryModes() {
        return deliveryService.getAllDeliveryModes();
    }

    /**
     * Retrieves a delivery by its ID.
     * @param id the ID of the delivery to retrieve.
     * @return a {@link reactor.core.publisher.Mono} emitting the DeliveryDTO of the delivery with the specified ID.
     *         If no delivery is found, a {@link com.rmila.deliverykata.presentation.exception.DeliveryModeNotFoundException} is thrown.
     */
    @GetMapping("/{id}")
    public Mono<DeliveryDTO> getDeliveryById(@PathVariable Long id) {
        return deliveryService.getDeliveryById(id).switchIfEmpty(Mono.error(new DeliveryModeNotFoundException(id)));
    }

}
