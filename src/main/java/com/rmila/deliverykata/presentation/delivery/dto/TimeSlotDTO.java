package com.rmila.deliverykata.presentation.delivery.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TimeSlotDTO {
    private Long id;
    private LocalDateTime endTime;
    private LocalDateTime startTime;
    private Long deliveryId;
    private Long reservedById;
}