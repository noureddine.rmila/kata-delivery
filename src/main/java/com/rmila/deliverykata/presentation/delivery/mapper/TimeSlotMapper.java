package com.rmila.deliverykata.presentation.delivery.mapper;

import com.rmila.deliverykata.domain.delivery.TimeSlot;
import com.rmila.deliverykata.presentation.delivery.dto.TimeSlotDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface TimeSlotMapper {
    TimeSlotDTO timeSlotToDTO(TimeSlot timeSlot);
    TimeSlot timeSlotDTOToTimeSlot(TimeSlotDTO timeSlotDTO);
}