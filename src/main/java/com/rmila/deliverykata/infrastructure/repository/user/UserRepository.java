package com.rmila.deliverykata.infrastructure.repository.user;

import com.rmila.deliverykata.domain.user.User;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;

public interface UserRepository  extends ReactiveCrudRepository<User, Long> {
}
