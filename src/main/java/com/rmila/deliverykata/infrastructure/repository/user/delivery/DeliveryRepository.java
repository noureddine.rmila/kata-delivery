package com.rmila.deliverykata.infrastructure.repository.user.delivery;

import com.rmila.deliverykata.domain.delivery.Delivery;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface  DeliveryRepository extends ReactiveCrudRepository<Delivery, Long> {
}
