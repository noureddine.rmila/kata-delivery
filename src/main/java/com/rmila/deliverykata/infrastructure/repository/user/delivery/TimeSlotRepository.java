package com.rmila.deliverykata.infrastructure.repository.user.delivery;

import com.rmila.deliverykata.domain.delivery.TimeSlot;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

@Repository
public interface TimeSlotRepository extends ReactiveCrudRepository<TimeSlot, Long> {
    Flux<TimeSlot> findByDeliveryIdAndReservedByIdIsNull(Long deliveryModeId);
}
