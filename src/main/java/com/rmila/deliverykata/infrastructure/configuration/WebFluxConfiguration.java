package com.rmila.deliverykata.infrastructure.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.r2dbc.repository.config.EnableR2dbcRepositories;
import org.springframework.web.reactive.config.EnableWebFlux;
import org.springframework.web.reactive.config.WebFluxConfigurer;

@Configuration
@EnableWebFlux
@EnableR2dbcRepositories
public class WebFluxConfiguration implements WebFluxConfigurer {
}
