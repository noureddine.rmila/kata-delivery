package com.rmila.deliverykata.application.service;

import com.rmila.deliverykata.application.port.timeSlot.TimeSlotServiceInputPort;
import com.rmila.deliverykata.infrastructure.repository.user.delivery.TimeSlotRepository;
import com.rmila.deliverykata.presentation.delivery.dto.TimeSlotDTO;
import com.rmila.deliverykata.presentation.delivery.mapper.TimeSlotMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple2;
import reactor.util.function.Tuples;

/**
 * Provides time slot-related services.
 * This class implements the {@link com.rmila.deliverykata.application.port.timeSlot.TimeSlotServiceInputPort} interface.
 * It retrieves time slot-related information from the repository and maps them to DTOs for presentation.
 * @author Noureddine Rmila
 */
@RequiredArgsConstructor
@Service
public class TimeSlotServiceAdapter implements TimeSlotServiceInputPort {

    private final TimeSlotRepository timeSlotRepository;
    private final TimeSlotMapper timeSlotMapper;

    /**
     * Retrieves available time slots for a given delivery mode.
     * @param id the ID of the delivery mode.
     * @return a {@link reactor.core.publisher.Flux} emitting TimeSlotDTOs.
     */
    @Override
    public Flux<TimeSlotDTO> getAvailableTimeSlotsByMode(Long id) {
        return timeSlotRepository.findByDeliveryIdAndReservedByIdIsNull(id)
                .map(timeSlotMapper::timeSlotToDTO);
    }

    /**
     * Reserves a time slot for a user.
     * @param timeSlotId the ID of the time slot to reserve.
     * @param userId the ID of the user reserving the time slot.
     * @return a {@link reactor.core.publisher.Mono} emitting a Tuple2 containing a boolean indicating whether the reservation was successful
     *         and a string message describing the result.
     */
    @Override
    public Mono<Tuple2<Boolean, String>> reserveTimeSlot(Long timeSlotId, Long userId) {
        return timeSlotRepository.findById(timeSlotId)
                .flatMap(timeSlot -> {
                    if (timeSlot != null) {
                        if (timeSlot.getReservedById() == null) {
                            timeSlot.setReservedById(userId);
                            return timeSlotRepository.save(timeSlot).map(saved -> Tuples.of(true, "Time slot reserved successfully."));
                        } else {
                            return Mono.just(Tuples.of(false, "Time slot is already reserved."));
                        }
                    } else {
                        return Mono.just(Tuples.of(false, "Time slot not found."));
                    }
                });
    }

}
