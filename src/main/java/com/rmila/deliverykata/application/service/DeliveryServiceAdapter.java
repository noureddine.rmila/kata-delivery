package com.rmila.deliverykata.application.service;

import com.rmila.deliverykata.application.port.delivery.DeliveryServiceInputPort;
import com.rmila.deliverykata.infrastructure.repository.user.delivery.DeliveryRepository;
import com.rmila.deliverykata.presentation.delivery.dto.DeliveryDTO;
import com.rmila.deliverykata.presentation.delivery.mapper.DeliveryMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Provides delivery-related services.
 * This class implements the {@link com.rmila.deliverykata.application.port.delivery.DeliveryServiceInputPort} interface.
 * It retrieves delivery-related information from the repository and maps them to DTOs for presentation.
 * @author Noureddine Rmila
 */
@RequiredArgsConstructor
@Service
public class DeliveryServiceAdapter implements DeliveryServiceInputPort {

    private final DeliveryRepository deliveryRepository;
    private final DeliveryMapper deliveryMapper;

    /**
     * Retrieves all delivery modes.
     * @return a {@link reactor.core.publisher.Flux} emitting DeliveryDTOs.
     */
    @Override
    public Flux<DeliveryDTO> getAllDeliveryModes() {
        return deliveryRepository.findAll()
                .map(deliveryMapper::deliveryToDTO);
    }

    /**
     * Retrieves a delivery by its ID.
     * @param id the ID of the delivery.
     * @return a {@link reactor.core.publisher.Mono} emitting the DeliveryDTO of the delivery with the specified ID,
     *         or empty if no delivery is found.
     */
    @Override
    public Mono<DeliveryDTO> getDeliveryById(Long id) {
        return deliveryRepository.findById(id)
                .map(deliveryMapper::deliveryToDTO);
    }

}
