/**
 * Represents the output port for time slot-related services.
 */
package com.rmila.deliverykata.application.port.timeSlot;

import reactor.core.publisher.Mono;
import reactor.util.function.Tuple2;

public interface TimeSlotServiceOutputPort {

    /**
     * Handles the result of a time slot reservation.
     * @param success Indicates if the reservation was successful
     * @param message The message indicating the result
     * @return A mono containing a tuple indicating handling success and message
     */
    Mono<Tuple2<Boolean, String>> handleTimeSlotReservation(Boolean success, String message);
}
