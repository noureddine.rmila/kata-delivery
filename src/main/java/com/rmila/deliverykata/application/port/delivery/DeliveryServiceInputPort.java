/**
 * Represents the input port for delivery-related services.
 */
package com.rmila.deliverykata.application.port.delivery;

import com.rmila.deliverykata.presentation.delivery.dto.DeliveryDTO;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface DeliveryServiceInputPort {

    /**
     * Retrieves all delivery modes.
     * @return A flux of delivery DTOs
     */
    Flux<DeliveryDTO> getAllDeliveryModes();

    /**
     * Retrieves a delivery by its ID.
     * @param id The ID of the delivery
     * @return A mono containing the delivery DTO
     */
    Mono<DeliveryDTO> getDeliveryById(Long id);
}
