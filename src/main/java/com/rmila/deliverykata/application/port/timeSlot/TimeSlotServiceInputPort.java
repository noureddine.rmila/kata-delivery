/**
 * Represents the input port for time slot-related services.
 */
package com.rmila.deliverykata.application.port.timeSlot;

import com.rmila.deliverykata.presentation.delivery.dto.TimeSlotDTO;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple2;

public interface TimeSlotServiceInputPort {

    /**
     * Retrieves available time slots for a specific mode.
     * @param id The ID of the mode
     * @return A flux of time slot DTOs
     */
    Flux<TimeSlotDTO> getAvailableTimeSlotsByMode(Long id);

    /**
     * Reserves a time slot for a user.
     * @param timeSlotId The ID of the time slot
     * @param userId The ID of the user
     * @return A mono containing a tuple indicating reservation success and message
     */
    Mono<Tuple2<Boolean, String>> reserveTimeSlot(Long timeSlotId, Long userId);
}
