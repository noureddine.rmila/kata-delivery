    package com.rmila.deliverykata.domain.delivery;

    public enum DeliveryMode {
        DRIVE,
        DELIVERY,
        DELIVERY_TODAY,
        DELIVERY_ASAP
    }