package com.rmila.deliverykata.domain.delivery;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table("delivery_modes")
public class Delivery {
    @Id
    private Long id;

    private DeliveryMode name;

    private String description;
}