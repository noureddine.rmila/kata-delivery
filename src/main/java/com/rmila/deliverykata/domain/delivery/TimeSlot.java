package com.rmila.deliverykata.domain.delivery;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

import java.time.LocalDateTime;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table("time_slots")
public class TimeSlot {
    @Id
    private Long id;

    @Column("end_time")
    private LocalDateTime endTime;

    @Column("start_time")
    private LocalDateTime startTime;

    @Column("delivery_id")
    private Long deliveryId;

    @Column("reserved_by_id")
    private Long reservedById;

}
